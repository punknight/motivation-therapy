const assert = require('assert');
const puppeteer = require('puppeteer');
var CREDS = require('../.docs/creds');
var MODAL_SELECTOR = '#LoginAcct';
var EMAIL_SELECTOR = '#container > div > div.modal > div > section.modal-body > input:nth-child(2)';
var PASSWORD_SELECTOR = '#container > div > div.modal > div > section.modal-body > input:nth-child(5)';
var SUBMIT_SELECTOR = '#container > div > div.modal > div > section.modal-body > button';

let browser
let page

before(async () => {
  browser = await puppeteer.launch()
  page = await browser.newPage()
});

describe('user login', ()=>{
	it('checks the login functionality from the browser', async ()=>{
		await page.goto('file:///Users/solinari/Websites/motivation-therapy/_pages/index.html');
		await page.click(MODAL_SELECTOR);
		await page.focus(EMAIL_SELECTOR);
		await page.keyboard.type(CREDS.email);
		await page.focus(PASSWORD_SELECTOR);
		await page.keyboard.type(CREDS.password);
		 await page.screenshot({ path: 'screenshots/login.png' });
		await page.click(SUBMIT_SELECTOR);
		await page.waitFor(2*1000);
		//await page.waitForNavigation();
		var test_text = await page.evaluate(() => document.getElementById('test').innerHTML);
		await page.screenshot({ path: 'screenshots/result.png' });
		console.log(test_text);
		assert(test_text == 'Login Successful');
	}).timeout(10000)
})

after(async () => {
  await browser.close()
})