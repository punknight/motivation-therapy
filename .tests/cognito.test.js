var assert = require('assert');
var Cognito = require('../_scripts/cognito');
var creds = require('../.docs/creds');
var fetch = require('isomorphic-fetch');
var AWSCognito = {
	CognitoIdentityServiceProvider: require("amazon-cognito-identity-js")
};



describe('Cognito class', function(){
	var cognito = new Cognito(AWSCognito.CognitoIdentityServiceProvider);
	describe('constructor', function(){
		it('should create a cognito class', function(done){
			assert(typeof cognito.poolData == 'object');
			assert(typeof cognito.CognitoUserPool == 'function');
  		assert(typeof cognito.CognitoUserAttribute == 'function');
  		assert(typeof cognito.CognitoUser == 'function');
  		assert(typeof cognito.AuthenticationDetails == 'function');
			done();	
		});
	});

	describe('doLogin', function(){
		it('should login a user', function(done){
			var email_input = { value: creds.email };
			var password_input = { value: creds.password };
			cognito.doLogin(email_input, password_input, function(err, accessToken){
				assert(err===null);
				assert(typeof accessToken == 'string');
				done();
			})	
		})
	});
});