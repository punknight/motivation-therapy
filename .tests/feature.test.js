var assert = require('assert');
var Feature = require('../_scripts/obj-models').Feature;
var path = require('path');
var file = path.join(__dirname, '../_data/feature_data.csv');

var feature;
describe('feature', function(){
	describe('constructor', function(){
		it('should create a feature with an arr, a schema, and a file attribute', function(done){
			feature = new Feature(file);
			assert(typeof feature.list == 'function');
			assert(typeof feature.add == 'function');
			assert(typeof feature.validate == 'function');
			assert(feature.hasOwnProperty('file'));
			assert(feature.hasOwnProperty('arr'));
			assert(feature.hasOwnProperty('schema'));
			done();
		});
	});
	describe('schema', function(){
		it('should get the encouraged schema for the feature_arr', function(done){
			var found_index = feature.schema.indexOf('feature_id');
			assert(found_index!= -1);
			done();
		});
	});
	describe('validate', function(){
		it('validate schema and return null errs', function(done){
			feature.list(function(err, feature_farr){
				feature.validate(feature_farr, function(err, feature_varr){
					assert(err===null);
					done();
				});
			});
		});
	});
});