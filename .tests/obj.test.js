var assert = require('assert');
var Obj = require('../_scripts/obj');
var path = require('path');
var file = path.join(__dirname, '../_data/obj_arr.csv');

var obj;
describe('Obj', function(){
	describe('constructor', function(){
		it('should create an obj with an arr and file attribute', function(done){
			obj = new Obj(file);		
			assert(obj.hasOwnProperty('file'));
			assert(obj.hasOwnProperty('arr'));
			done();
		});
	})
	describe('list', function(){
		it('should list the file contents as an obj', function(done){
			obj.list(function(err, obj_arr){
				assert(err == null);
				assert(typeof obj_arr != 'undefined');
				assert(obj_arr.length === obj.arr.length);
				done();
			});
		})
	})
	describe('add', function(){
		it('should add an object to the file contents', function(done){
			var old_length = obj.arr.length;
			obj.add({id: 999}, function(err, new_arr){
				assert(new_arr.length == (old_length+1));
				assert(new_arr.length === obj.arr.length);
				done();	
			})
		})
	})
})