var http = require('http');
var fs = require('fs');
var path = require('path');
var file = path.join(__dirname, '/.tasks');

var server = http.createServer(function(req, res){
	switch(req.method){
		case 'GET':
			console.log('received get request\n');
			res.statusCode = 200;
			res.setHeader('Content-Type', 'text/plain');
			listTasks(file, function(err, task_arr){
				if (err) console.error(err);
				if(task_arr.length===0){
					res.end('no tasks\n');
				} else {
					task_arr.forEach((todo_item, todo_index)=>{
						res.write(`${todo_index+1}: ${todo_item}\n`);
					});
					res.end();
				}
			});
			
			break;
		case 'POST':
			var task_str = '';
			req.setEncoding('utf-8');
			req.on('data', function(chunk){
				console.log(`parsed task as: ${chunk}`);
				task_str += chunk;
			});
			req.on('end', function(){
				console.log('done parsing');
				addTask(file, task_str);
				res.statusCode = 204;
				res.end('data received');
			});
			break;
		default:
			res.statusCode = 405;
			res.end('bad request\n');
			break;
	}
}).listen(process.env.PORT || 3000, function(){
	console.log('server running on env var PORT or 3000');
});

function loadOrInitializeTaskArray(file, cb){
	fs.exists(file, function(exists){
		var tasks = [];
		if(exists){
			fs.readFile(file, 'utf8', function(err, data){
				if(err) throw err;
				var data = data.toString();
				var tasks = JSON.parse(data || '[]');
				cb(tasks);
			});
		} else {
			cb([]);
		}
	});
}

function listTasks(file, cb){
	loadOrInitializeTaskArray(file, function(task_arr){
		cb(null, task_arr)
	});
}

function storeTasks(file, tasks){
	fs.writeFile(file, JSON.stringify(tasks), 'utf8', function(err){
		if(err) throw err;
		console.log('Saved.');
	});
}

function addTask(file, taskDescription){
	loadOrInitializeTaskArray(file, function(tasks){
		tasks.push(taskDescription);
		storeTasks(file, tasks);
	});
}