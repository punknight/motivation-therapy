//https://medium.com/code-monkey/object-composition-in-javascript-2f9b9077b5e6
//assign:
//takes parameter objects
//returns a single object with all of their combined attributes
const canCast = (state) => ({
    cast: (spell) => {
        console.log(`${state.name} casts ${spell}!`);
        state.mana--;
    }
});

const canFight = (state) => ({
    fight: () => {
        console.log(`${state.name} slashes at the foe!`);
        state.stamina--;
    }
})

const canFightBetter = (state) => ({
    fight: () => {
        console.log(`${state.name} slashes and burns at the foe!`);
        state.stamina -= 2;
    }
})

const fighter = (name) => {
  let state = {
    name,
    health: 100,
    stamina: 100
  }
  
  return Object.assign(state, canFight(state));
}

const mage = (name) => {
  let state = {
    name,
    health: 100,
    mana: 100
  }
  
  return Object.assign(state, canCast(state));
}

scorcher = mage('Scorcher')
scorcher.cast('fireball');    // Scorcher casts fireball!
console.log(scorcher.mana)    // 99

slasher = fighter('Slasher')
slasher.fight();              // Slasher slashes at the foe!
console.log(slasher.stamina)  // 99


const paladin = (name) => {
  let state = {
    name,
    health: 100,
    mana: 100,
    stamina: 100
  }
  
  return Object.assign(state, canCast(state), canFight(state));
}

roland = paladin('Roland');   
roland.fight();               // Roland slashes at the foe!
roland.cast('Holy Light');    // Roland casts Holy Light!

const BadAss = (name) => {
  let state = {
    name,
    health: 100,
    mana: 100,
    stamina: 100
  }
  return Object.assign(state, canCast(state), canFight(state), canFightBetter(state))
}

krunk = BadAss('Krunk');   
krunk.fight();               
console.log('does krunk fight or fight better?')
console.log(krunk)  // fight better