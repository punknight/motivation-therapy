var http = require('http');
var fs = require('fs');
var parse = require('url').parse;

//create a socket.io app that adds user when they register
//creates an action for registering
//pulls from the feature csv to define the action

var server = http.createServer(function(req, res){
	if(req.method != 'GET'){
		res.statusCode = 405;
		res.end('bad request\n');
	} 
	switch(req.url.substr(0, 2)){
		case '/':
			var stream = fs.createReadStream(__dirname+'/_pages/index.html');
			stream.pipe(res);
			break;
		case '/.':
			res.statusCode = 403;
			res.end('forbidden\n');
			break;
		case '/_':
			var path = __dirname+req.url;
			fs.stat(path, function(err, stat){
				if(err){
					if('ENOENT' == err.code){
						res.statusCode=404;
						res.end('Resource Not Found\n');
					} else {
						res.statusCode = 500;
						res.end('Internal Server Error');
					}
				} else {
					res.statusCode = 200;
					var stream = fs.createReadStream(path);
					stream.pipe(res);
				}
			});
			break;
		case '/favicon.ico':
			res.statusCode=404;
			res.end();
			break;
		default:
			res.statusCode = 404;
			res.end(`no directory:  ${req.url}\n`);
	}
}).listen(process.env.PORT || 3000, function(){
	console.log('file server up and running on env port or 3000');
});