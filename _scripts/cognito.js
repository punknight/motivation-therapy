function Cognito(ext){
  this.CognitoUserPool = ext.CognitoUserPool;

  this.CognitoUserAttribute = ext.CognitoUserAttribute;
  this.CognitoUser = ext.CognitoUser;
  this.AuthenticationDetails = ext.AuthenticationDetails;
  //create the user pool
  this.poolData = {
    UserPoolId : 'us-east-1_qd0HN1c3n', 
    ClientId : '7ta7n7t67aregn1g6pjisffefp' 
  };
};
  
Cognito.prototype.doRegister = function(value1, value2){
	//get the input values;
  var email = value1.value;
  var password = value2.value;
	var userPool = new this.CognitoUserPool(this.poolData);
	
	//create the attribute list
	var attributeList = [];
  var dataEmail = {
      Name : 'email',
      Value : email // your email here
  };
  var attributeEmail = new this.CognitoUserAttribute(dataEmail);
  attributeList.push(attributeEmail);
  //call the signup function
  userPool.signUp(email, password, attributeList, null, function(err, result){
    if (err) {
        console.error(err);
    } else {
      cognitoUser = result.user;
      console.log('user registered as ' + cognitoUser.getUsername());  
    }
  });
}
Cognito.prototype.doConfirm = function(value1, value2){
	//get the input values;
  var email = value1.value;
  var confirm_code = value2.value;
	var userPool = new this.CognitoUserPool(this.poolData);
	var userData = {
    Username: email,
    Pool: userPool
  };
  var cognitoUser = new this.CognitoUser(userData);
  cognitoUser.confirmRegistration(confirm_code, true, function(err, result) {
    if (err) {
        console.error(err);
        return;
    }
    console.log('call result: ' + result);
  });
}

Cognito.prototype.doLogin = function(value1, value2, cb){
  var email = value1.value;
  var password = value2.value;
  var userPool = new this.CognitoUserPool(this.poolData);
  var userData = {
    Username: email,
    Pool: userPool
  };
  var authenticationData = {
      Username : email,
      Password : password
  };
  var authenticationDetails = new this.AuthenticationDetails(authenticationData);
  var cognitoUser = new this.CognitoUser(userData);
  cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: function (result) {
          var accessToken = result.getAccessToken().getJwtToken();
          //console.log("accessToken: ", accessToken);
          cb(null, accessToken);
          
      },

      onFailure: function(err) {
        console.error(err);
        cb(err);
          
      },
      mfaRequired: function(codeDeliveryDetails) {
          var verificationCode = prompt('Please input verification code' ,'');
          cognitoUser.sendMFACode(verificationCode, this);
      }
  });
  
}


if (typeof module !== 'undefined' && typeof module.exports !== 'undefined'){
  module.exports = Cognito;
} else {
  window.Cognito = Cognito;
}