var fs = require('fs');

function Obj(file, arr){
	//this is a class for creating, listing, and storing any type of object_arr in file storage
	this.file = file;
	this.arr = arr || [];
}
//helper functions
function loadOrInitializeObjArray(file, cb){
	fs.exists(file, function(exists){
		if(exists){
			fs.readFile(file, 'utf8', function(err, data){
				if(err) throw err;
				var data = data.toString();
				var obj_arr = JSON.parse(data || '[]');
				cb(obj_arr);
			});
		} else {
			cb([]);
		}
	});
}
//private functions
function doLoad(obj, cb){
	//load data into .arr attribute
	loadOrInitializeObjArray(obj.file, function(obj_arr){
		obj.arr = obj_arr;
		return cb(obj);
	});
}
function doStore(obj, cb){
	//store data from .arr attribute
	fs.writeFile(obj.file, JSON.stringify(obj.arr), 'utf8', function(err){
		if(err) throw err;		
		return cb(obj);
	});
}

Obj.prototype.list = function(cb){
	doLoad(this, function(loaded_obj){
		return cb(null, loaded_obj.arr);	
	})
}

Obj.prototype.add = function(new_obj, cb){
	doLoad(this, function(loaded_obj){		
		loaded_obj.arr.push(new_obj);
		doStore(loaded_obj, function(stored_obj){
			return cb(null, stored_obj.arr);
		});
	});
}

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined'){
  module.exports = Obj;
} else {
  
  window.Obj = Obj;
}

