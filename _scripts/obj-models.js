var assert = require('assert');
var Obj = require('../_scripts/obj');
var path = require('path');
var file = path.join(__dirname, '../_data/feature_data.csv');

const featureSchema = (state) => ({
  schema: [
  'feature_id', 
  'hypothesis', 
  'test_metric', 
  'value_before', 
  'value_after', 
  'before_date',
  'after_date',
  'web_test',
  'android_test',
  'iphone_test'
  ],
  validate: (loaded_arr, cb) => {
    var err = null;
    var feature_arr = loaded_arr.map((feature_obj)=>{
      if( !feature_obj.hasOwnProperty('feature_id')) err+= 'feature_obj needs a valid id\n';
			if( !feature_obj.hasOwnProperty('hypothesis')) err+= 'feature_obj needs hypothesis\n';
			if( !feature_obj.hasOwnProperty('test_metric')) err+='feature_obj needs test_metric\n';
			if( !feature_obj.hasOwnProperty('test_arr')) err+= 'feature_obj needs test_arr\n';
			return feature_obj;
		});
		this.arr = feature_arr;
    cb(err, feature_arr);
  },
});

exports.Feature =  function(file){
  let state = {
    file,
    arr: []
  }
  var test = new Obj(file);
  
  return Object.assign(new Obj(file), featureSchema(state));
}



